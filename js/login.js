$(function(){
  $('#myform').submit(function(event) {
    event.preventDefault();

    jQuery.support.cors = true;

    var frm = $(this);
    var dat = JSON.stringify(frm.serializeArray()
    .reduce(function(a, x) { a[x.name] = x.value; return a; }, {})); 

    $.ajax({
        type: "POST",
        url: "http://localhost:8080/restaurant_manager/logins/verify",
        header: { "access-control-allow-origin" : "*"},
        data: dat,
        success: function(data){
          if(data.role=="waiter"){
            window.location="index.html";
          }

          if(data.role=="kitchen"){
            window.location="kitchen.html";
          }
        },
        error : function(){
          alert("Invalid username or password!!!");
        },
        dataType: "json",
        contentType : "application/json"
     });
  });   
});