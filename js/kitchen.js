$(document).ready(
	function() {

		jQuery.support.cors = true;

		$.ajax({
			type : "GET",
			url : "http://192.168.1.107:8080/restaurant_manager/order/",
			dataType : "json",
			success : function(data) {

				var trHTML = '';

				$.each(data, function(i, item) {

					trHTML += '<tr><td>' + data[i].idOrder + '</td><td>'
							+ data[i].id_table + '</td><td>'
							+ data[i].client.name + '</td><td>'
							+ data[i].ready + '</td><td>'
							+ '<button value="' + data[i].idOrder + '">Mark as ready</button></td></tr>';
				});

				$("#orders_table").append(trHTML);
				// console.log(JSON.stringify(data));

				$("button").click(function() {
						jQuery.support.cors = true;
						
						 $.ajax({
							 type : "GET",
							 url : "http://192.168.1.107:8080/restaurant_manager/order/" + $(this).attr("value"),
							 dataType : "json",
							 success : function(data) { 
								window.location = "kitchen.html";
							 },
						
							 error : function(sls, msg) {
						
								 console.log(sls.responseText);
								 window.location.href = "index.html";
							 }
						 });
					});

			},

			error : function(sls, msg) {

				console.log(sls.responseText);
				//window.location.href = "index.html";
			}
		});

		$.ajax({
			type : "GET",
			url : "http://192.168.1.107:8080/restaurant_manager/orderedMenus/",
			dataType : "json",
			success : function(data) {

				var trHTML = '';

				$.each(data, function(i, item) {

					trHTML += '<tr><td>' + data[i].menu.idMenu + '</td><td>'
							+ data[i].menu.name + '</td><td>'
							+ data[i].order.id_table + '</td><td>'
							+ data[i].quantity + '</td></tr>';
				});

				$("#ordered_menu_table").append(trHTML);
				// console.log(JSON.stringify(data));
			},

			error : function(sls, msg) {

				console.log(sls.responseText);
				//window.location.href = "index.html";
			}
		});
		
	});