$(function () {
	$("#menu_name").text($.cookie("menu"));
});

$(function(){
	$("#order_button").click(function(){
		var idMenu = $.cookie("idMenu");
		var idClient = $("#client_name option:selected").attr('value');
		var tableNo = $("#table_no option:selected").text();
		var quantity = $("#quantity option:selected").text();

		var data = {
			"idMenu" : parseInt(idMenu),
			"idClient" : parseInt(idClient),
			"tableNo" : parseInt(tableNo),
			"quantity" : parseInt(quantity)
		}

		$.ajax({
			 type : "POST",
			 url : "http://localhost:8080/restaurant_manager/order/",
			 contentType: "application/json",
			 dataType : "json",
			 data : JSON.stringify(data),
			 success : function(data) { 
				alert("Order complete! Email will be send!");
			 },

			 error : function(sls, msg) {

				 console.log(sls.responseText);
			 }
			});
	});
});